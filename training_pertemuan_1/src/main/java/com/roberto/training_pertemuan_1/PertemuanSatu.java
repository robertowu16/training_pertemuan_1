/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.training_pertemuan_1;

import java.util.*;
import java.lang.Math;

/**
 *
 * @author asus
 */
public class PertemuanSatu {

    final static Scanner INPUT = new Scanner(System.in);

    public static void main(String[] args) {

        // No. 1
        Scanner input = new Scanner(System.in);
//        int value = input.nextInt();
//
//        for (int a = 2; a <= value; a++) {
//            int mod = 1;
//            for (int b = 2; b < a; b++) {
//                if (a % b == 0) {
//                    mod = 0;
//                }
//            }
//            if (mod == 1) {
//                System.out.print(a + " ");
//            }
//        }
//        System.out.println();
//
//        // No. 2
//        double result = 0;
//        for (var a = 1; a <= 7; a++) {
//            result += Math.pow(a, 2);
//            System.out.printf("%.0f ", result);
//        }

//        // No. 3
//        int add = 1;
//        int temp = 2;
//        for (int start = 0; start <=4; start++) {
//            double result2 = Math.pow(temp, 3);
//            temp = temp + add;
//            System.out.printf("%.0f ", result2);
//            add++;
//        }
        // No. 4
        List<Integer> even = new ArrayList<>();
        List<Integer> odd = new ArrayList<>();
        System.out.println("Start = ");
        int start = input.nextInt();
        System.out.println("End = ");
        int end = input.nextInt();

        System.out.println("Size awal = " + even.size());
        for (int loop = start; start <= end; loop++) {
            if (loop % 2 == 0) {
                even.add(loop);
            } else {
                odd.add(loop);
            }
        }

        System.out.println("Size akhir = " + even.size());
//        System.out.println("Genap:");
//        even.forEach((a) -> {
//            System.out.println(a);
//        });
//        System.out.println("");
//        System.out.println("Ganjil:");
//        odd.forEach((a) -> {
//            System.out.println(a);
//        });
    }
}
